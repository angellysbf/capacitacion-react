import React from 'react';
import styled from 'styled-components'; 
import Card from './card';
import Submenu from './submenu';


const Container = styled.div`
  ` 

const App = () =>{

  const data= [
    {
      color: "#21d0d0",
      options: [
        {title: "PRICE LOW TO HIGH", state: false},
        {title: "PRICE HIGH TO LOW",  state: false}, 
        {title: "POPULARITY",  state: true}, 
      ]
    },

      {
      color: "#ff7745",
      options: [
        {title: "1up nutrition", state: false},
        {title: "asitis",  state: false}, 
        {title: "avvatar",  state: true}, 
        {title: "big muscle",  state: false},
        {title: "bpi sports",  state: false}, 
        {title: "bsn",  state: false}, 
        {title: "cellucor",  state: false}, 
        {title: "domin8r",  state: false}, 


 

      ]
    }
    ,
  ];

  return(
    <Container>

      <Card/>

      {data.map((v, i) => <Submenu {...v}/>)}

    </Container>
  ); 
}


export default App;
