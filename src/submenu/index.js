import React from 'react';
import styled from 'styled-components'; 
import Options, {Circulo, Icon} from './option'
import Cancel from '../images/x.png'; 
import Check from '../images/checkazul.png'; 



import Option from './option'

const Container = styled.div`
    height:200px; 
    width: 400px; 
    border-radius: 25px; 
    padding-top: 15px; 
    background-color: ${props => props.color};
  ` 
  const Footer = styled.div`
    display: flex; 
    flex-direction: row; 
    justify-content: center;
    align-items: center;  
  ` 
  const App = (params) =>{
    return(
        <Container color={params.color}>

            {params.options.map((v, i) => <Option key={i} {...v}/>)}

            <Footer>

                {
                    [Cancel, Check].map((v, i) => {
                        return(

                            <Circulo visible={true} style={{height:60, width: 60}}>

                                <Icon src={v} visible={true} style={{height:60, width: 60}}/>

                            </Circulo>
                        )
                })

                }

            </Footer>
            
        </Container>
    ); 
  }
  export default App;