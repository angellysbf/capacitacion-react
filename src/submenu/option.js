import React, {useState, useEffect} from 'react';
import styled from 'styled-components'; 
import Check from '../images/checkazul.png'; 


const Container = styled.div`
        
    margin-left: 25px;
    margin-top: 15px;
    display: flex; 
    flex-direction: row; 
    justify-content: space-between;
    margin-right: 25px;

  ` 
const Title= styled.p`
    margin: 0;
    color: white;
    font-weight: 700;
    font-size: 15px; 
    text-transform: uppercase;
  
`
export const Circulo = styled.div`

    border-radius: 50%; 
    border: 1.8px solid white; 
    height:35px; 
    width: 35px; 
    cursor: pointer; 
    background-color: ${props => props.visible ? 'white': 'transparent'};
  ` ;
export const Icon = styled.img`
    height:35px; 
    width: 35px; 
    object-fit: contain; 
    border-radius: 50%;
    visibility: ${props => props.visible ? 'visible': 'hidden'};

  `;


  const App = ({title, state}) =>{

    const [visible, setVisible]= useState(false);

    useEffect(() =>{

        setVisible(state)
        
    }, [state]);

    const handleChange = () =>{
        setVisible(!visible);
    };

    return(
        <Container>
    
            <Title>
                {title}
            </Title>

            <Circulo visible={visible} onClick={handleChange}>
                <Icon src={Check} visible={visible}/>
            </Circulo>

        </Container>
    ); 
  }
  export default App;