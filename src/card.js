import React from 'react';
import styled from 'styled-components'; 

const Container = styled.div`
    height:150px; 
    width: 350px; 
    border-radius: 5px; 
    background-color: #fff0ea; 
    display: flex; 
    flex-direction: column; 
    justify-content: center;
    align-items: center;  
  ` 
const Title= styled.p`
   
    color: #ff8b60;
    font-weight: bold;
    font-size: 15px; 
    text-transform: uppercase;
    width: 250px;
    text-align: center; 
`
const Button = styled.button`
    background-color: #f06f37; 
    text-transform: uppercase;
    border: 1px solid  #f06f37; 
    padding: 7px  25px; 
    color: white; 
    border-radius: 15px; 
    cursor: pointer;  
`
  const App = () =>{

    return(
        <Container>
    
            <Title>
                Use code: pigi100 for rs.100 off on your first order!
            </Title>

            <Button>
                grab now
            </Button>

        </Container>
    ); 
  }
  export default App;